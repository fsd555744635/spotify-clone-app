let search = document.getElementById("searching");
let list_div = document.getElementsByClassName("list")[0];
let parent = document.getElementById('main-songs');
let errMsg = document.getElementById('errorMessage');

function searching_button() {

    if (search.value.toLowerCase().trim() === "arjunreddy") {
        parent.innerHTML = "";
        event.preventDefault();
        list_div.innerHTML = "";

        // Create a div element with class "item"
        const itemDiv = document.createElement('div');
        itemDiv.className = 'item';

        // Create an img element and set its "src" attribute
        const imgElement = document.createElement('img');
        imgElement.src = 'https://img1.hotstarext.com/image/upload/f_auto,t_vl/sources/r1/cms/prod/9217/1389217-v-c29b53bc1557';

        // Create a div element with class "play"
        const playDiv = document.createElement('div');
        playDiv.className = 'play';

        // Create a span element with class "fa fa-play"
        const playIcon = document.createElement('span');
        playIcon.className = 'fa fa-play';

        // Create an h4 element for the title
        const titleH4 = document.createElement('h4');
        titleH4.textContent = "Arjun Reddy";

        // Create a p element for the description
        const descriptionP = document.createElement('p');
        descriptionP.textContent = "Telisiney Na Nuvvey";

        // Create a div element with class "song"
        const songDiv = document.createElement('div');
        songDiv.className = 'song';

        // Create an audio element with controls, loop, and muted attributes
        const audioElement = document.createElement('audio');
        audioElement.controls = true;
        audioElement.loop = true;
        audioElement.muted = true;
        audioElement.style.cssText = 'position:relative; padding: 0px; width: 250px;height: 30px;';

        // Create a source element for the audio
        const sourceElement = document.createElement('source');
        sourceElement.src = '[iSongs.info] 04 - Telisiney Na Nuvvey.mp3';

        playDiv.appendChild(playIcon);
        songDiv.appendChild(audioElement);
        audioElement.appendChild(sourceElement);
        itemDiv.appendChild(imgElement);
        itemDiv.appendChild(playDiv);
        itemDiv.appendChild(titleH4);
        itemDiv.appendChild(descriptionP);
        itemDiv.appendChild(songDiv);

        list_div.appendChild(itemDiv);



    }

    else if (search.value.toLowerCase().trim() === "darling") {

        parent.innerHTML = "";
        event.preventDefault();
        list_div.innerHTML = "";


        // Create a div element with class "item"
        const itemDiv = document.createElement("div");
        itemDiv.classList.add("item");

        // Create an img element and set its src attribute
        const img = document.createElement("img");
        img.src = "https://i.pinimg.com/474x/2e/05/33/2e053377112fe367701e568f6832e134.jpg";

        // Create a div element with class "play"
        const playDiv = document.createElement("div");
        playDiv.classList.add("play");

        // Create a span element with class "fa fa-play"
        const playSpan = document.createElement("span");
        playSpan.classList.add("fa", "fa-play");

        // Create an h4 element with text content "REbel melodies"
        const h4 = document.createElement("h4");
        h4.textContent = "Darling";

        // Create a p element with text content "prabhas and kajol classic hits..."
        const p = document.createElement("p");
        p.textContent = "prabhas and kajol classic hits";

        // Create a div element with class "song"
        const songDiv = document.createElement("div");
        songDiv.classList.add("song");

        // Create an audio element with controls, loop, and muted attributes
        const audio = document.createElement("audio");
        audio.controls = true;
        audio.loop = true;
        audio.muted = true;
        audio.style.cssText = "position:relative; padding: 0px; width: 250px;height: 30px;";

        // Create a source element with src attribute
        const source = document.createElement("source");
        source.src = "[iSongs.info] 05 - Pranama.mp3";

        playDiv.appendChild(playSpan);
        songDiv.appendChild(audio);
        audio.appendChild(source);
        itemDiv.appendChild(img);
        itemDiv.appendChild(playDiv);
        itemDiv.appendChild(h4);
        itemDiv.appendChild(p);
        itemDiv.appendChild(songDiv);

        list_div.appendChild(itemDiv);

    }

    else if (search.value.toLowerCase().trim() === "rx100") {
        parent.innerHTML = "";
        event.preventDefault();
        list_div.innerHTML = "";
        // Create a div element with class "item"
        const itemDiv = document.createElement('div');
        itemDiv.className = 'item';

        // Create an img element and set its source attribute
        const img = document.createElement('img');
        img.src = 'https://www.thetelugufilmnagar.com/wp-content/uploads/2021/06/Pilla-ra.jpg';

        // Create a div element with class "play" and add a span element with class "fa fa-play"
        const playDiv = document.createElement('div');
        playDiv.className = 'play';
        const playSpan = document.createElement('span');
        playSpan.className = 'fa fa-play';
        playDiv.appendChild(playSpan);

        // Create an h4 element and set its text content
        const h4 = document.createElement('h4');
        h4.textContent = 'RX 100';

        // Create a p element and set its text content
        const p = document.createElement('p');
        p.textContent = 'Rock Legends & epic songs that continue t...';

        // Create a div element with class "song" and add an audio element with controls
        const songDiv = document.createElement('div');
        songDiv.className = 'song';
        const audio = document.createElement('audio');
        audio.controls = true;
        audio.loop = true;
        audio.muted = true;
        audio.style.cssText = 'position:relative; padding: 0px; width: 250px;height: 30px;';
        const source = document.createElement('source');
        source.src = '[iSongs.info] 03 - Pillaa Raa.mp3';
        audio.appendChild(source);
        songDiv.appendChild(audio);


        itemDiv.appendChild(img);
        itemDiv.appendChild(playDiv);
        itemDiv.appendChild(h4);
        itemDiv.appendChild(p);
        itemDiv.appendChild(songDiv);


        list_div.appendChild(itemDiv);

    }
    else if (search.value.toLowerCase().trim() === "petta") {
        parent.innerHTML = "";
        event.preventDefault();
        list_div.innerHTML = "";
        // Create a new div element for the container with class "item"
        const itemDiv = document.createElement('div');
        itemDiv.classList.add('item');

        // Create an img element and set its src attribute
        const img = document.createElement('img');
        img.setAttribute('src', 'https://static.toiimg.com/photo/67464901.cms?imgsize=57540');

        // Create a div element for the play button with class "play"
        const playDiv = document.createElement('div');
        playDiv.classList.add('play');

        // Create a span element for the play icon with class "fa fa-play"
        const playIcon = document.createElement('span');
        playIcon.classList.add('fa', 'fa-play');

        // Create an h4 element for the title
        const h4 = document.createElement('h4');
        h4.textContent = 'Petta';

        // Create a p element for the description
        const p = document.createElement('p');
        p.textContent = 'Massu Maranam';

        // Create a div element for the audio player with class "song"
        const audioDiv = document.createElement('div');
        audioDiv.classList.add('song');

        // Create an audio element with controls, loop, and muted attributes
        const audio = document.createElement('audio');
        audio.setAttribute('controls', '');
        audio.setAttribute('loop', '');
        audio.setAttribute('muted', '');
        audio.style.cssText = 'position:relative; padding: 0px; width: 250px;height: 30px;';

        // Create a source element for the audio file
        const source = document.createElement('source');
        source.setAttribute('src', '[iSongs.info] 01 - Massu Maranam.mp3');

        playDiv.appendChild(playIcon);
        audio.appendChild(source);
        audioDiv.appendChild(audio);
        itemDiv.appendChild(img);
        itemDiv.appendChild(playDiv);
        itemDiv.appendChild(h4);
        itemDiv.appendChild(p);
        itemDiv.appendChild(audioDiv);

        list_div.appendChild(itemDiv);

    }
    else if (search.value.toLowerCase().trim() === "bigil") {
        parent.innerHTML = "";
        event.preventDefault();
        list_div.innerHTML = "";
        // Create a div element with class "item"
        const itemDiv = document.createElement('div');
        itemDiv.className = 'item';

        // Create an img element with a src attribute
        const img = document.createElement('img');
        img.src = 'https://files.prokerala.com/movies/pics/800/movie-wallpaper-107705.jpg';

        // Create a div element with class "play"
        const playDiv = document.createElement('div');
        playDiv.className = 'play';

        // Create a span element with class "fa fa-play"
        const playIcon = document.createElement('span');
        playIcon.className = 'fa fa-play';

        // Create an h4 element with text content "Deep Focus"
        const h4 = document.createElement('h4');
        h4.textContent = 'Bigil';

        // Create a p element with text content "Keep calm and focus with ambient and pos..."
        const p = document.createElement('p');
        p.textContent = 'Verrekkiddam';

        // Create a div element with class "song"
        const songDiv = document.createElement('div');
        songDiv.className = 'song';

        // Create an audio element with controls, loop, and muted attributes
        const audio = document.createElement('audio');
        audio.controls = true;
        audio.loop = true;
        audio.muted = true;
        audio.style = 'position:relative; padding: 0px; width: 250px;height: 30px;';

        // Create a source element with src attribute for the audio
        const source = document.createElement('source');
        source.src = '[iSongs.info] 01 - Verrekkiddam.mp3';

        playDiv.appendChild(playIcon);
        songDiv.appendChild(audio);
        audio.appendChild(source);
        itemDiv.appendChild(img);
        itemDiv.appendChild(playDiv);
        itemDiv.appendChild(h4);
        itemDiv.appendChild(p);
        itemDiv.appendChild(songDiv);

        list_div.appendChild(itemDiv);

    }
    else if (search.value.toLowerCase().trim() === "sikindar") {
        parent.innerHTML = "";
        event.preventDefault();
        list_div.innerHTML = "";
        // Create a div element with class "item"
        const itemDiv = document.createElement('div');
        itemDiv.classList.add('item');

        // Create an img element and set its src attribute
        const imgElement = document.createElement('img');
        imgElement.src = 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ7n168Ih76-VqFMRBFK12allJ_Yy-wtvrkcuOwcDwhsvOo8JceOg8t9AmiMvAo6MqWNhg&usqp=CAU';

        // Create a div element with class "play"
        const playDiv = document.createElement('div');
        playDiv.classList.add('play');

        // Create a span element with class "fa fa-play"
        const playSpan = document.createElement('span');
        playSpan.classList.add('fa', 'fa-play');

        // Create an h4 element with text "Instrumental Study"
        const h4Element = document.createElement('h4');
        h4Element.textContent = 'Sikindar';

        // Create a p element with text "Focus with soft study music in the..."
        const pElement = document.createElement('p');
        pElement.textContent = 'Focus with soft study music in the...';

        // Create a div element with class "song"
        const songDiv = document.createElement('div');
        songDiv.classList.add('song');

        // Create an audio element with controls, loop, and muted attributes
        const audioElement = document.createElement('audio');
        audioElement.controls = true;
        audioElement.loop = true;
        audioElement.muted = true;
        audioElement.style.cssText = 'position:relative; padding: 0px; width: 250px; height: 30px;';

        // Create a source element with src attribute
        const sourceElement = document.createElement('source');
        sourceElement.src = '[iSongs.info] 02 - Chitike.mp3';

        playDiv.appendChild(playSpan);
        songDiv.appendChild(audioElement);
        audioElement.appendChild(sourceElement);
        itemDiv.appendChild(imgElement);
        itemDiv.appendChild(playDiv);
        itemDiv.appendChild(h4Element);
        itemDiv.appendChild(pElement);
        itemDiv.appendChild(songDiv);



        list_div.appendChild(itemDiv);
    }
    else {
        parent.innerHTML = "";
        errMsg.innerHTML = "Your search result is not there";
    }
}